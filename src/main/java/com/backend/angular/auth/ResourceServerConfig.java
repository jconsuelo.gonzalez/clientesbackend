package com.backend.angular.auth;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        //se parte desde lo mas especifico, el ultimo authenticated es para todas las que no esten especificadas (soloque este autenticado)
        //Se pueden administrar los permisos desde el controlador también por eso se comenta y lo emos alla, y se tiene que habilitar en el SpringSecurityConfig con anotacion EnableGlobalMethodSecurity
        http.authorizeRequests().antMatchers(HttpMethod.GET, "/api/clientes", "/api/clientes/page/**", "/api/uploads/img/**", "/images/**").permitAll() // ojo con el metodo authorizeRequest, suele confundirse con el authorizeHttpRequest y genera error
                /*antMatchers(HttpMethod.GET, "/api/clientes/{id}").hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.POST, "/api/clientes/upload").hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.POST, "/api/clientes").hasRole("ADMIN") //hasRole: un solo rol, hasAnyRol: mas de un rol
                .antMatchers("/api/clientes/**").hasRole("ADMIN")*///generico para update y delete (y para cualquier otro que no esté especificado mas arriba, por ejemplo la api de las regiones)
                .anyRequest().authenticated()
                .and().cors().configurationSource(corsConfigurationSource());//esta linea se agrega por la integración de Spring security Cors de abajo
    }


    //modificamos el CORS de Spring Security Cors, para solucionar el problema de access conrtol allow origin HEADER, podemos configurar para varios cleintes (rutas)
    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("http://localhost:4200"));
        configuration.setAllowedMethods(Arrays.asList("GET","POST", "PUT", "DELETE", "OPTIONS"));// podemos aplicarlo par todos con *, igual arriba
        configuration.setAllowCredentials(true);
        configuration.setAllowedHeaders(Arrays.asList("content-type", "Authorization"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    //se crea un filtro, se registra lo de arriba y le damos la prioridad mas alta
    @Bean
    public FilterRegistrationBean<CorsFilter> corsFilter(){
        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(corsConfigurationSource()));
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return bean;
    }
}
