package com.backend.angular.models.services;

import com.backend.angular.models.entity.Usuario;

public interface IUsuarioService {

    public Usuario findBuUsername(String username);
}
