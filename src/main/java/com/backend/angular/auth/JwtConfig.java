package com.backend.angular.auth;

public class JwtConfig {

    public static final String LLAVE_SECRETA = "alguna.clave.secreta.12345678";


    /*las llaves se obtuvieron instalando openssl, luegho se agregaron al registro de variables de entorno de windows, luego: openssl genrsa -out jwt.pem, seguido de: openssl rsa -in jwt.pem */

    public static final String RSA_PRIVADA = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEpQIBAAKCAQEAr/sE+eil+P8DpQaQvhIxgRVfxOy/nHr7tYqm5p2uJQY8lnUm\n" +
            "tnbXA3zQ6Fp6ctJXeshL9fAoxZQkdUoS4Zr9J/5JbaBDDQo/4IjjaGIE12nlwEUp\n" +
            "0yPq+X3Bg/gPUsT0D9zuAVAOSstZLXAnVqH6bTh0Wbr2f5bDPY+BKLsPLuO65+9o\n" +
            "wLUu+8GjZ8D4VvZJrwgBh1xPX2/5gOonM69iWCD9ZelrUXwIxj0Kst0J2yF7ldf1\n" +
            "VGyQbRmgi++/P+cle0VFVXHhz9shEwqMR84gTErnokCq63oP4MQ2v0IpbYy2ViMn\n" +
            "jL7mJCAYYpl/j+KeIqsLbuRXAFZHInW9csU8/wIDAQABAoIBAQCYu2SrPPZnb3SL\n" +
            "UnJm9EVQwes3bz4xoi85dVe7SVitu4b0EkMCE4wy864gKv6hDttrrdD3Z3MLX7Jw\n" +
            "tVRDx481maXC5lerRpz/0+6CVqfbCYakY6KUJFU4KaO9VJp//H3SEq3iJ8fvtCL2\n" +
            "o6MQtk4xYtDgW7q6K2/Ryu2PGyJnTz7tJOd1U6XfIZjiJpc2JHQ9WxjG+1lvV7gY\n" +
            "WVS0tZgPHxeSnfoBQphoJcZlLGMhyF1to2C7vZemLEk1Qchw9WxkHmzussAyh86L\n" +
            "Kdzqan1fFk0oWl1xcJYiZNf2xFmnXJN2SvUM7F6DXSBQUw84A/lCzs21FIvr9Mss\n" +
            "c374d65xAoGBAOZL2+kZLhOpFdUYMzjWLeptWR5NEI10/tEUvAiUF1VXvIOhZIKU\n" +
            "bT3iMiFO9HtKMEmPzP66oRe2zLp25+VnQ7bOt/DlNRYxmWFuvu4yEzymMZ1MYVQW\n" +
            "bJkoJTineML8SDeU/7cw6+KKTSg8vD6dvZSUNmsPWX83wYzk2EPwUIKXAoGBAMOf\n" +
            "ORT8HW+8cR/oFUT9C9roTPRZLNuUpJYcX/ctqg84wU5AOs4yQoxs83uP+gAkhUUr\n" +
            "f8wjzQN3wNigenuVQNh52gFDbwGvUJU1YqExlDzMN6bL68xbiAWgJ6g+A9PSXvOX\n" +
            "QbtsW7bgkso1cuSszN2iYn63hcLyd25rXfihPi3ZAoGBAKhobu9dt4EvE8UaBlMY\n" +
            "5Mjan+H1rxYSqUjSJdMMIKgLGdMUUcDfs6ceY1RSPZxvGzKgEuNwLN9/kOUGkPwD\n" +
            "8NOdBYwGwTRyntxONRdQzRp72ipSMZZhM5cocVeluDy2akUjE4CuKMEoK9Yx2+La\n" +
            "TUl6vPS7a9IJPuO+cZv0/5sPAoGANxGOjyK94qOIsPOzcR1PBgvZBfxIjXPVtVA9\n" +
            "j6ayNeDDQhUAuOUbKM7O6BzKwtmFDsjFDCGT6AMBLbpXYIzOmWIMBckG6dGBfM9v\n" +
            "eXMsiMTwcar8MSYfL347S7hReYLCKg6NeYZydxPXHsdXFZqzfTzmP0fIhu2cw1/E\n" +
            "L2asy1kCgYEArxXt6ga8f8KPy+Iyzbb2t+LLC9EdRxXHlupdTNlevjmGVL2Ya2SM\n" +
            "NcraeM9XTksQQTkPA/wAfOOEkFNm1aLJ3RuyA5h6fjtWVuCLzNaaGL7JOhSuy5F6\n" +
            "7vwEj2F9PbO4EjLydjB/iIPewLIzcSeWdOZxL0ytU+0WtG0mL/qpLSA=\n" +
            "-----END RSA PRIVATE KEY-----";

    public static final String RSA_PUBLICA = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAr/sE+eil+P8DpQaQvhIx\n" +
            "gRVfxOy/nHr7tYqm5p2uJQY8lnUmtnbXA3zQ6Fp6ctJXeshL9fAoxZQkdUoS4Zr9\n" +
            "J/5JbaBDDQo/4IjjaGIE12nlwEUp0yPq+X3Bg/gPUsT0D9zuAVAOSstZLXAnVqH6\n" +
            "bTh0Wbr2f5bDPY+BKLsPLuO65+9owLUu+8GjZ8D4VvZJrwgBh1xPX2/5gOonM69i\n" +
            "WCD9ZelrUXwIxj0Kst0J2yF7ldf1VGyQbRmgi++/P+cle0VFVXHhz9shEwqMR84g\n" +
            "TErnokCq63oP4MQ2v0IpbYy2ViMnjL7mJCAYYpl/j+KeIqsLbuRXAFZHInW9csU8\n" +
            "/wIDAQAB\n" +
            "-----END PUBLIC KEY-----";
}
