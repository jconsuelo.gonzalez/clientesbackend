package com.backend.angular.models.dao;

import com.backend.angular.models.entity.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface IUsuarioDao extends CrudRepository<Usuario, Long> {

    //public Usuario findByUsernameAndEmail(String username, String email);//ejemplo de buscar por mas de 1 parametro

    public Usuario findByUsername(String username);

    //ejemplo con mas de 1 parametro
    //@Query("select u from Usuario u where u.username = ?1 and u.email=?2")
    //public Usuario findByUsername2(String username, String email);

    @Query("select u from Usuario u where u.username = ?1")
    public Usuario findByUsername2(String username);
}
