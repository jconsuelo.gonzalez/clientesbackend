insert into regiones (id, nombre) values(1, 'Sudamérica');
insert into regiones (id, nombre) values(2, 'Centroamérica');
insert into regiones (id, nombre) values(3, 'Norteamérica');


insert into cliente (region_id, nombre, apellido, email, create_at) values (1, 'Miguel', 'Chacana', 'mchacana@gmail.com', '2022-04-25');
insert into cliente (region_id, nombre, apellido, email, create_at) values (2, 'Daniel', 'Gonzalez', 'dgonzalez@hotmail.com', '2021-05-05');
insert into cliente (region_id, nombre, apellido, email, create_at) values (3, 'Pablo', 'Jimenez', 'pjimenez@gmail.com', '2022-04-02');

--Creamos algunos usuarios con sus roles, para datos de prueba, la clave nos e pone pues debe ir encryptada la sacamos de la consola con el metodo que tenemos en el main
insert into usuarios(username, password, enabled, nombre, apellido, email) values('andres', '$2a$10$rtejkBswAh20AWg5L2LgO.K4hFLbj9486nYz/ydAIJf.EUqWwOzFq', 1, 'Manuel', 'Duran', 'mdurans@dgtm.cl');
insert into usuarios(username, password, enabled, nombre, apellido, email) values('admin', '$2a$10$YltI.gQZXl6T3.wiq0HKSegazZGi6YG9Q8x3p548iVT.E4B4V6zZu', 1, 'Priscila', 'Mendoza', 'pmendoza@dgtm.cl');

insert into roles(nombre) values('ROLE_USER');
insert into roles(nombre) values('ROLE_ADMIN');

insert into usuarios_roles(usuario_id, role_id) values(1,1);
insert into usuarios_roles(usuario_id, role_id) values(2,2);
insert into usuarios_roles(usuario_id, role_id) values(2,1);



